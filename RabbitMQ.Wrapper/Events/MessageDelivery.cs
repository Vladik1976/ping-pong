﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper.Events
{
    public class MessageDelivery
    {
        public MessageDelivery(BasicDeliverEventArgs e)
        {
            Body = e.Body;
            DeliveryTag = e.DeliveryTag;
        }
        public ReadOnlyMemory<byte> Body { get; private set; }

        public ulong DeliveryTag {get; private set;}

    }
}
