﻿
namespace RabbitMQ.Wrapper.Enums
{
    public enum ExchangeTypeEnum : byte
    {
        Direct = 1,
        Fanout,
        Headers,
        Topic,
    }
}
