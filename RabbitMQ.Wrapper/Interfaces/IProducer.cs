﻿using RabbitMQ.Wrapper.Settings;
using System;
using System.Collections.Generic;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IProducer
    {
        void SendMessageToQueue(ExchangeSettings exchangeSettings,
                            string message,
                            Dictionary<string, object> messageAttributes,
                            string timeToLive = "30000");

    }
}
