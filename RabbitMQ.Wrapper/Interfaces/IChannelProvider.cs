﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IChannelProvider : IDisposable
    {
        IModel GetChannel();
    }
}
