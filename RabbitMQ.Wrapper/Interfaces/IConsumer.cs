﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Events;
using RabbitMQ.Wrapper.Settings;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IConsumer
    {
        public string ListenQueue(ExchangeSettings exchangeSettings, QueueSettings queueSettings, bool autoAck, Action<MessageDelivery> CallBack);
    }
}
