﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using System;

namespace RabbitMQ.Wrapper.Implementations
{
    public class ChannelProvider : IChannelProvider
    {
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private IModel _channel;

        public ChannelProvider(string hostName= "localhost", string url= "amqp://guest:guest@localhost:5672", string username= ConnectionFactory.DefaultUser, 
                                string password= ConnectionFactory.DefaultPass)
        {
            _connectionFactory = new ConnectionFactory()
            {
                UserName = password,
                Password = username,
                HostName = hostName,
                Uri = new Uri(url), 
            };

             _connection = _connectionFactory.CreateConnection();
        }

        public IModel GetChannel()
        {
            _channel = _connection.CreateModel();
           // _channel.BasicConsume();
            return _channel;
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();

            _channel.Dispose();
            _connection.Dispose();
        }
               
    }
}
