﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Settings;
using System;

namespace RabbitMQ.Wrapper.Implementations
{
    public class Consumer : IConsumer
    {
        private readonly ChannelProvider _channelProvider;
        private IModel channel;
        public Consumer(ChannelProvider channelProvider)
        {
            _channelProvider = channelProvider;
        }
        public string ListenQueue(ExchangeSettings exchangeSettings, QueueSettings queueSettings,bool autoAck, Action<MessageDelivery> CallBack)
        {
            channel = _channelProvider.GetChannel();

            channel.ExchangeDeclare(exchangeSettings.Name,
                exchangeSettings.Type.ToString().ToLower(),
                exchangeSettings.Durable,
                exchangeSettings.AutoDelete);

            channel.QueueDeclare(queueSettings.Name,
                queueSettings.Durable,
                queueSettings.Exclusive,
                queueSettings.AutoDelete,
                null);
            channel.QueueBind(queueSettings.Name, exchangeSettings.Name, exchangeSettings.RoutingKey);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender,e) => 
            {

                CallBack(new MessageDelivery(e));
                channel.BasicAck(e.DeliveryTag, false);
            };

           return channel.BasicConsume(queueSettings.Name, autoAck, consumer);
        }

    }
}
