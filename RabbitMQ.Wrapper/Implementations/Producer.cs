﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Settings;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.Implementations
{
    public class Producer : IProducer
    {
        private readonly IChannelProvider _channelProvider;
        private readonly IModel _channel;
        public Producer(IChannelProvider channelProvider)
        {
            _channelProvider = channelProvider;
            _channel = _channelProvider.GetChannel();
        }

        public void SendMessageToQueue(ExchangeSettings exchangeSettings, string message, Dictionary<string, object> messageAttributes = null, string timeToLive = "30000")
        {
            byte[] messageBodyBytes = Encoding.UTF8.GetBytes(message);

            _channel.ExchangeDeclare(exchangeSettings.Name,
                exchangeSettings.Type.ToString().ToLower(),
                exchangeSettings.Durable,
                exchangeSettings.AutoDelete);

            IBasicProperties basicProperties = _channel.CreateBasicProperties();

            if (messageAttributes is not null)
            {
                basicProperties.Headers = messageAttributes;
            }


            _channel.BasicPublish(exchangeSettings.Name, exchangeSettings.RoutingKey, basicProperties, messageBodyBytes);
        }

        public void Dispose()
        {
            _channelProvider.Dispose();
        }

    }
}
