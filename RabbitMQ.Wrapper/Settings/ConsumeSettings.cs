﻿using System.Collections.Generic;

namespace RabbitMQ.Wrapper.Settings
{
    public class ConsumerSettings
    {
        public bool AutoAcknowledge { get; set; }
        public Dictionary<string,object> ConsumeAttributes { get; set; }
    }
}
