﻿using RabbitMQ.Wrapper.Enums;

namespace RabbitMQ.Wrapper.Settings
{
    public class ExchangeSettings
    {
        public string Name { get; set; }
        public ExchangeTypeEnum Type { get; set; }
        public bool Durable { get; set; }
        public bool AutoDelete { get; set; }
        public string RoutingKey { get; set; }
    }
}
