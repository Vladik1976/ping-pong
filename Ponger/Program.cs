﻿using RabbitMQ.Wrapper.Enums;
using RabbitMQ.Wrapper.Events;
using RabbitMQ.Wrapper.Implementations;
using RabbitMQ.Wrapper.Settings;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Ponger
{
    class Program
    {
        private static ExchangeSettings _exchangeSettings;

        static void Main(string[] args)
        {
            InitializeConsumer();
            InitializeProducer();
            Console.ReadKey();
        }
        private static void SendMessage(string message)
        {
            using (ChannelProvider _channelProvider = new())
            {
                Producer _producer = new(_channelProvider);
                _producer.SendMessageToQueue(_exchangeSettings, message, null, "30000");
            }
        }

        private static void InitializeConsumer()
        {
            QueueSettings _queueSettings = new();
                _queueSettings.Name = "pong_queue";
                _queueSettings.Durable = true;
                _queueSettings.AutoDelete = false;
                _queueSettings.Exclusive = false;

            ExchangeSettings _exchangeSettings = new();
                _exchangeSettings.AutoDelete = false;
                _exchangeSettings.Durable = true;
                _exchangeSettings.Name = "pingpong";
                _exchangeSettings.Type = ExchangeTypeEnum.Direct;
                _exchangeSettings.RoutingKey = "out";

            ChannelProvider _channelProvider = new();
            Consumer _consumer = new(_channelProvider);
            _consumer.ListenQueue(_exchangeSettings, _queueSettings, false, ProcessMessage);
        }

        private static void InitializeProducer()
        {
            _exchangeSettings = new();
                _exchangeSettings.AutoDelete = false;
                _exchangeSettings.Durable = true;
                _exchangeSettings.Name = "pingpong";
                _exchangeSettings.Type = ExchangeTypeEnum.Direct;
                _exchangeSettings.RoutingKey = "in";
        }

        private static void ProcessMessage(MessageDelivery messageDelivery)
        {
            var message = Encoding.UTF8.GetString(messageDelivery.Body.ToArray());

            Console.WriteLine(DateTime.Now.ToString() + " Received: " + message);

            Task.Delay(2500).ContinueWith(t => SendMessage("pong"));
        }
    }
}
